const jwt = require("jsonwebtoken");
const createError = require("http-errors");
function verifyToken(request, response, next) {
  const bearerHeader = request.headers["authorization"];
  if (request.method == "GET") {
    next();
  } else {
    if (typeof bearerHeader !== "undefined") {
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer[1];
      request.token = bearerToken;
      jwt.verify(request.token, "secretkey", (err, authData) => {
        if (err) {
          next(createError(403));
        } else {
          next();
        }
      });
    } else {
      next(createError(403));
    }
  }
}

module.exports = verifyToken;
