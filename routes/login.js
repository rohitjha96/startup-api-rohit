const express = require("express");
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const db = require("../db/dbQueries");
const validators = require("../validators/validators");
let router = express.Router();
router.post("/", (req, res) => {
  let user = {};
  const saltRounds = 10;
  user["password"] = req.body.password;
  user["email"] = req.body.email;
  if (validators.validateUser(user)) {
    res.send(validators.validateUser(user).details[0].message);
  } else {
    bcrypt.genSalt(saltRounds, function(err, salt) {
      bcrypt.hash(user["password"], salt, function(err, hash) {
        db.addUserInformation(user.email, hash)
          .then(result => {
            res.json(user.email);
          })
          .catch(err => res.send("user already registered"));
      });
    });
  }
});
module.exports = router;
