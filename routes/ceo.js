const express = require("express");
const bodyParser = require("body-parser");
let router = express.Router();

const db = require("../db/dbQueries");
const validators = require("../validators/validators");
const verifyToken = require("../middleware/authentication/jwt");
const createError = require("http-errors");
router.use(express.static("public"));

router.get("/", getAllCeos);
router.post("/", addCeo);
router.get("/:id", getCeoById);
router.put("/:id", updateCeo);
router.delete("/:id", deleteCeo);

function deleteCeo(request, response, next) {
  let idOfCeoToBeDeleted = request.params.id;
  db.fetchCeoById(idOfCeoToBeDeleted).then(ceo => {
    if (ceo.length == 0) {
      next(createError(404));
    } else {
      db.deleteCeo(idOfCeoToBeDeleted)
        .then(result => {
          response.json(result);
        })
        .catch(error => next(createError(500)));
    }
  });
}
function addCeo(request, response, next) {
  let newCeo = request.body;
  if (validators.validateCeo(newCeo)) {
    return response.json(validators.validateCeo(newCeo));
  }
  db.addNewCeo(newCeo)
    .then(result => {
      newCeo["id"] = result.insertId;
      response.json(newCeo);
    })
    .catch(error => {
      next(createError(500));
    });
}
function updateCeo(request, response, next) {
  let updatedCeo = request.body;
  let ceoId = request.params.id;
  let validator = validators.validateCeo(updatedCeo);
  if (validator) {
    return response.json(validator["details"]);
  }
  db.fetchCeoById(ceoId).then(ceo => {
    if (ceo.length == 0) {
      next(createError(404));
    } else {
      db.updateCeo(updatedCeo, ceoId)
        .then(queryResolved => {
          response.json(updatedCeo);
        })
        .catch(err => next(createError(500)));
    }
  });
}
function getCeoById(request, response, next) {
  db.fetchCeoById(request.params.id)
    .then(result => {
      if (result.length != 0) {
        if (request.headers["content-type"] == "application/json") {
          response.json(result);
        } else {
          response.render("ceo", {
            result
          });
        }
      } else {
        next(createError(404));
      }
    })
    .catch(error => next(createError(500)));
}
function getAllCeos(request, response, next) {
  db.fetchAllCeos()
    .then(result => {
      if (request.headers["content-type"] == "application/json") {
        response.json(result);
      } else {
        response.render("ceo", {
          result
        });
      }
    })
    .catch(error => next(createError(500)));
}

module.exports = router;
