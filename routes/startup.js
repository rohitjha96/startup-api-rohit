const express = require("express");
const bodyParser = require("body-parser");

const db = require("../db/dbQueries");
const validators = require("../validators/validators");
const verifyToken = require("../middleware/authentication/jwt");
const createError = require("http-errors");
let router = express.Router();
router.use(express.static("public"));

router.get("/", getAllStartups);
router.get("/:id", getStartupById);
router.post("/", addStartup);
router.put("/:id", updateStartup);
router.delete("/:id", deleteStartup);

function getAllStartups(request, response, next) {
  db.fetchAllStartups()
    .then(result => {
      if (request.headers["content-type"] == "application/json") {
        response.json(result);
      } else {
        response.render("startup", {
          result
        });
      }
    })
    .catch(error => next(createError(500)));
}
function getStartupById(request, response, next) {
  db.fetchStartUpById(request.params.id)
    .then(result => {
      if (result.length != 0) {
        if (request.headers["content-type"] == "application/json") {
          response.json(result);
        } else {
          response.render("startup", {
            result
          });
        }
      } else {
        next(createError(404));
      }
    })
    .catch(error => next(createError(500)));
}
function addStartup(request, response, next) {
  let newStartup = request.body;
  let validator = validators.validateNewStartup(newStartup);
  if (validator) {
    return response.json(validator["details"]);
  }
  db.addNewStartup(newStartup)
    .then(result => {
      newStartup["id"] = result.insertId;
      response.json(newStartup);
    })
    .catch(error => {
      next(createError(500));
    });
}
function updateStartup(request, response, next) {
  let updatedStartup = request.body;
  let startupId = request.params.id;
  let validator = validators.validateUpdateStartup(updatedStartup);
  if (validator) {
    return response.json(validator["details"]);
  }
  db.fetchStartUpById(startupId).then(startup => {
    if (startup.length == 0) {
      next(createError(404));
    } else {
      db.updateStartup(updatedStartup, startupId)
        .then(result => {
          response.json(updatedStartup);
        })
        .catch(err => next(createError(500)));
    }
  });
}
function deleteStartup(request, response, next) {
  let idOfStartupToBeDeleted = request.params.id;
  db.fetchStartUpById(idOfStartupToBeDeleted).then(startup => {
    if (startup.length == 0) {
      next(createError(404));
    } else {
      db.deleteStartup(idOfStartupToBeDeleted)
        .then(result => {
          response.json(result);
        })
        .catch(error => next(createError(500)));
    }
  });
}

module.exports = router;
