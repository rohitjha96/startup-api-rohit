const express = require("express");
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const db = require("../db/dbQueries");
const validators = require("../validators/validators");
const createError = require("http-errors");
let router = express.Router();
router.post("/", (req, res, next) => {
  let user = {};
  const saltRounds = 10;
  user["password"] = req.body.password;
  user["email"] = req.body.email;
  if (validators.validateUser(user)) {
    res.send(validators.validateUser(user).details[0].message);
  } else {
    bcrypt.genSalt(saltRounds, function(err, salt) {
      bcrypt.hash(user["password"], salt, function(err, hash) {
        db.fetchUserInformation(user.email).then(result => {
          if (result.length != 0) {
            jwt.sign({ user: user }, "secretkey", (error, token) => {
              res.send("\ntoken = " + token);
            });
          } else {
            next(createError(401));
          }
        });
      });
    });
  }
});

module.exports = router;
