const Joi = require("joi");
function validateCeo(newCeo) {
  let schema = Joi.object().keys({
    name: Joi.string()
      .max(20)
      .required(),
    company: Joi.string()
      .max(20)
      .required()
  });
  const { error } = Joi.validate(
    { name: newCeo.name, company: newCeo.company },
    schema
  );
  return error;
}
function validateUser(user) {
  let schema = Joi.object().keys({
    email: Joi.string().required(),
    password: Joi.string().required()
  });
  const { error } = Joi.validate(
    {
      email: user.email,
      password: user.password
    },
    schema
  );
  return error;
}

function validateNewStartup(newStartup) {
  let schema = Joi.object().keys({
    name: Joi.string()
      .max(20)
      .required(),
    year: Joi.number()
      .integer()
      .required(),
    type: Joi.string()
      .max(20)
      .required(),
    country: Joi.string()
      .max(20)
      .required(),
    ceo: Joi.string()
      .max(20)
      .required()
  });
  const { error } = Joi.validate(
    {
      name: newStartup.name,
      year: newStartup.year,
      type: newStartup.type,
      country: newStartup.country,
      ceo: newStartup.ceo
    },
    schema
  );
  return error;
}
function validateUpdateStartup(newStartup) {
  let schema = Joi.object().keys({
    name: Joi.string().max(20),
    year: Joi.number().integer(),
    type: Joi.string().max(20),
    country: Joi.string().max(20),
    ceo: Joi.string().max(20)
  });
  const { error } = Joi.validate(
    {
      name: newStartup.name,
      year: newStartup.year,
      type: newStartup.type,
      country: newStartup.country,
      ceo: newStartup.ceo
    },
    schema
  );
  return error;
}
module.exports = {
  validateNewStartup,
  validateUpdateStartup,
  validateCeo,
  validateUser
};
