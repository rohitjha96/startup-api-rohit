require("dotenv").config();
let mysql = require("mysql");
let sql = mysql.createConnection({
  host: "localhost",
  user: process.env.user,
  password: process.env.password,
  database: "startupCeo",
  multupleStatements: true
});
function sqlQuery(query, parameter = []) {
  return new Promise((resolve, reject) => {
    sql.query(query, parameter, function(err, result) {
      if (err) {
        return reject(err);
      }
      return resolve(result);
    });
  });
}
function fetchAllCeos() {
  let query = "select * from ceo order by id";
  return sqlQuery(query);
}
function fetchCeoById(id) {
  let query = "select * from ceo where id=" + sql.escape(id);
  return sqlQuery(query);
}
function addNewCeo(newCeo) {
  let query =
    "insert into ceo(name,company) values(" +
    sql.escape(newCeo.name) +
    "," +
    sql.escape(newCeo.company) +
    ")";
  return sqlQuery(query);
}
function deleteCeo(idOfCeoToBeDeleted) {
  let query = "delete from ceo where id=" + sql.escape(idOfCeoToBeDeleted);
  return sqlQuery(query);
}
function fetchUserInformation(email) {
  let query = "select * from user where email=" + sql.escape(email);
  return sqlQuery(query);
}
function fetchAllStartups() {
  let query = "select * from startup order by id";
  return sqlQuery(query);
}
function addUserInformation(email, hashPassword) {
  let query =
    "insert into user(email,hashPassword) values (" +
    sql.escape(email) +
    "," +
    sql.escape(hashPassword) +
    ")";
  return sqlQuery(query);
}
function fetchStartUpById(id) {
  let query = "select * from startup where id=" + sql.escape(id);
  return sqlQuery(query);
}

function addNewStartup(newStartup) {
  let query =
    "insert into startup(name,year,type,country,ceo) values(" +
    sql.escape(newStartup.name) +
    "," +
    sql.escape(newStartup.year) +
    "," +
    sql.escape(newStartup.type) +
    "," +
    sql.escape(newStartup.country) +
    "," +
    sql.escape(newStartup.ceo) +
    ")";
  return sqlQuery(query);
}

function deleteStartup(idOfStartupToBeDeleted) {
  let query = "delete from startup where id=?";
  return sqlQuery(query, [idOfStartupToBeDeleted]);
}
function updateCeo(updatedCeo, ceoId) {
  return new Promise((resolve, reject) => {
    let queries = [];
    fetchCeoById(ceoId).then(result => {
      if (result.length == 0) {
        reject("Id not found");
      } else {
        if (updatedCeo.name != undefined) {
          let query =
            "update ceo set name=" +
            sql.escape(updatedCeo.name) +
            "where id=" +
            sql.escape(ceoId);
          queries.push(sqlQuery(query));
        }
        if (updatedCeo.company != undefined) {
          let query =
            "update ceo set company=" +
            sql.escape(updatedCeo.company) +
            "where id=" +
            sql.escape(ceoId);
          queries.push(sqlQuery(query));
        }
        resolve(Promise.all(queries));
      }
    });
  });
}
function updateStartup(updatedStartup, startupId) {
  return new Promise((resolve, reject) => {
    let queries = [];
    fetchStartUpById(startupId).then(result => {
      if (result.length == 0) {
        reject("Id not found");
      } else {
        if (updatedStartup.name != undefined) {
          let query =
            "update startup set name=" +
            sql.escape(updatedStartup.name) +
            "where id=" +
            sql.escape(startupId);
          queries.push(sqlQuery(query));
        }
        if (updatedStartup.year != undefined) {
          let query =
            "update startup set year=" +
            sql.escape(updatedStartup.year) +
            "where id=" +
            sql.escape(startupId);
          queries.push(sqlQuery(query));
        }
        if (updatedStartup.type != undefined) {
          let query =
            "update startup set type=" +
            sql.escape(updatedStartup.type) +
            "where id=" +
            sql.escape(startupId);
          queries.push(sqlQuery(query));
        }
        if (updatedStartup.country != undefined) {
          let query =
            "update startup set country=" +
            sql.escape(updatedStartup.country) +
            "where id=" +
            sql.escape(startupId);
          queries.push(sqlQuery(query));
        }
        if (updatedStartup.ceo != undefined) {
          let query =
            "update startup set ceo=" +
            sql.escape(updatedStartup.ceo) +
            "where id=" +
            sql.escape(startupId);
          queries.push(sqlQuery(query));
        }
        resolve(Promise.all(queries));
      }
    });
  });
}

module.exports = {
  fetchAllStartups,
  fetchStartUpById,
  addNewStartup,
  updateStartup,
  deleteStartup,
  fetchAllCeos,
  fetchCeoById,
  deleteCeo,
  addNewCeo,
  updateCeo,
  addUserInformation,
  fetchUserInformation
};
