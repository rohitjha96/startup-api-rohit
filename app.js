const express = require("express");
const bodyParser = require("body-parser");

const startupRoute = require("./routes/startup");
const ceoRoute = require("./routes/ceo");
const loginRoute = require("./routes/login");
const authorizeRoute = require("./routes/authorization");

const expressHandlebar = require("express-handlebars");
const logger = require("./middleware/logMiddleware/logMiddleware");

const httpErrorHandler = require("./middleware/http-errors/errorHandler");
const authenticate = require("./middleware/authentication/jwt");
const app = express();
app.listen(3000);

app.engine("handlebars", expressHandlebar({ defaultLayout: "main" }));
app.set("view engine", "handlebars");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(logger);

app.use(authenticate);
app.use("/api/startup", startupRoute);
app.use("/api/ceo", ceoRoute);
app.use("/api/login", loginRoute);
app.use("/api/authorize", authorizeRoute);
app.use(httpErrorHandler);
